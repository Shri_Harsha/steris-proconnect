//
//  Database.swift
//  SterisProConnect
//
//  Created by Shri Harsha on 21/12/17.
//  Copyright © 2017 Shri Harsha. All rights reserved.
//

import Foundation
import RealmSwift

class RealmDBService {
    
    static let sharedInstance = RealmDBService()
    private init () {}
    
    func insertDevice(deviceItemValue: Device) -> (Void){
        let realm = try! Realm()
        
        try! realm.write {
            
            // Add for fresh and Update existing once.
            realm.add(deviceItemValue, update: false)
        }
        
    }
    
    // Fetch results in Collection Object of type entity.
    func fetchDevices() -> (Results<Device>){
        
        // Fetches all realms in DB Memory, default realm in memory
        
        let realm = try! Realm()
        let deviceRealmResults = realm.objects(Device.self)
        
        return deviceRealmResults
    }
    
    func delete(primaryKey: Int) -> (Void){
        // To-Do.
    }
}

