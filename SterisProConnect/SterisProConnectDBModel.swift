//
//  SterisProConnectDBModel.swift
//  SterisProConnect
//
//  Created by Sreejith PA on 22/12/17.
//  Copyright © 2017 Altimetrik. All rights reserved.
//

import Foundation
import RealmSwift

class User: Object {
    dynamic var userRole = ""
    dynamic var userName = ""
    dynamic var lastModified = 0
}

class UserDeviceLink: Object {
    dynamic var userName = ""
    dynamic var linkType = ""
    dynamic var lastModified = 0
    dynamic var id = 0
    dynamic var deviceId = 0
}

class DeviceServiceHistory: Object {
    dynamic var srType = ""
    dynamic var srStatus = ""
    dynamic var srOpenDate = ""
    dynamic var srNumber = ""
    dynamic var srCloseDate = ""
    dynamic var lastModified = ""
    dynamic var id = 0
    dynamic var entryMode = ""
    dynamic var deviceId = 0
}

class Device: Object {
    dynamic var version = ""
    dynamic var serialNumber = ""
    dynamic var remainingCycleTime = ""
    dynamic var modelName = ""
    dynamic var localName = ""
    dynamic var lastModified = 0
    dynamic var inCycle = ""
    dynamic var id = 0
    dynamic var doorClosed = ""
    dynamic var deviceStatusTimestamp = ""
    dynamic var deviceStatus = ""
    dynamic var cyclePhase = ""
    dynamic var customerId = 0
    dynamic var currentCycleStatus = ""
    dynamic var connectionStatus = ""
}

class Customer: Object {
    dynamic var name = ""
    dynamic var lastModified = 0
    dynamic var id = 0
}

class Cycle: Object {
    dynamic var tapeCycleDateTime = ""
    dynamic var tapeContents = ""
    dynamic var lastModified = 0
    dynamic var id = 0
    dynamic var deviceId = 0
}

