//
//  ApiService.swift
//  SterisProConnect
//
//  Created by Shri Harsha on 20/12/17.
//  Copyright © 2017 Shri Harsha. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import MQTTClient

protocol ApiServiceProtocol {
    
    associatedtype ResponseData
    
    func authenticateUser(username: String, password: String, completion: @escaping(Data)->(Void)) -> (Void)
    func connectToMQTTBrokerWithAuthorizedData(authorizedData: Data , completion: @escaping (Bool) -> (Void))
}

class ApiService: ApiServiceProtocol {
    
    //Constant
    let loginApiPath = "http://174.138.73.254:8080/MobileLogin/Login?Content-Type=application/json&Accept=application/json"
    
    static let sharedInstance : ApiService = ApiService()
    private init () {}
    typealias ResponseData = Data
    
    func authenticateUser(username: String, password: String, completion: @escaping (Data) -> (Void)) {
        
        let requestURL = URL(string:loginApiPath)
        let parameters:Parameters = [
            "username": username,
            "password": password
        ]
        let credentialData = "\(String(describing: parameters["username"]!)):\(String(describing: parameters["password"]!))".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString()
        let headers = ["Authorization": "Basic \(base64Credentials)"]
        var statusCode: Int = 0
        
        Alamofire.request(requestURL!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { response in
                
                statusCode = (response.response?.statusCode)! //Gets HTTP status code, useful for debugging
                print(statusCode)
                switch response.result {
                case .success:
                    if let value = response.result.value {
                        let json = JSON(value)
                        print(json)
                    }
                case .failure(_):
                    if let error = response.result.error {
                        print(error)
                        break
                    }
                }
                completion(response.data!)
        }
    }
    
    func connectToMQTTBrokerWithAuthorizedData(authorizedData: Data , completion: @escaping (Bool) -> (Void)){
        
        if let jsonData = try! JSONSerialization.jsonObject(with:authorizedData , options: []) as? [String: Any]{
            print(jsonData)
            if((jsonData["result"]! as! Bool)) {
                
            }
            else{
                completion(false)
            }
        }
    }
    
    func fetchDataDump (completion: ([String:Any]?)->()){
        
        // Fetch the bigger JSON
        let parameters:Parameters = [
            "username": "demo3",
            "password": "steris456"
        ]
        
        completion (parameters)
    }
}
