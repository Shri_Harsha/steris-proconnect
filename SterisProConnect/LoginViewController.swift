//
//  LoginViewController.swift
//  SterisProConnect
//
//  Created by Sreejith PA on 22/12/17.
//  Copyright © 2017 Altimetrik. All rights reserved.
//

import UIKit
import LocalAuthentication
import MessageUI
import MaterialTextField
import SwiftyJSON
import MBProgressHUD



class LoginViewController: UIViewController, UITextFieldDelegate, MFMailComposeViewControllerDelegate {
    
// MARK:Contstants
    let kMsgShowFinger = "Touch your finger on home button"
    let kMsgShowReason = "Please Authentication"
    let kMsgFingerOK = "Login successful!"
    
    var usernameTxtCount = 0
    var passwordTxtCount = 0
    
    var context = LAContext()
    
    @IBOutlet weak var userNameTxt: MFTextField!
    @IBOutlet weak var passwordTxt: MFTextField!
    @IBOutlet weak var loginBtn: UIButton!
    
// MARK: View controller methods
    override func viewDidAppear(_ animated: Bool) {
        
        let deviceItem = Device()
        deviceItem.modelName = "TestDevice1"
         RealmDBService.sharedInstance.insertDevice(deviceItemValue: deviceItem)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        userNameTxt.delegate = self
        passwordTxt.delegate = self
        
        loginBtn.layer.cornerRadius = 20
        
        if ((userNameTxt.text?.isEmpty)! && (passwordTxt.text?.isEmpty)!){
            loginBtn.isEnabled = false;
        }
        
        print(KeychainService.loadKeyData(key:"AccessToken"))
        if KeychainService.loadKeyData(key:"AccessToken") != nil {
            invokeTouchIDAuthentication()
        }
        
//        let tempResult = RealmDBService.sharedInstance.fetchDevices()
//        print(tempResult)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // hide keyboard on touch outside
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // hide keyboard on return button
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
// MARK: IBActions
    @IBAction func usernameTextChanged(_ sender: Any) {

        if(userNameTxt.text?.count != 0 && passwordTxtCount != 0){
            loginBtn.isEnabled = true
        } else {
            loginBtn.isEnabled = false
        }
        usernameTxtCount = (userNameTxt.text?.count)!

    }
    
    @IBAction func passwordTextChanged(_ sender: Any) {
        if(passwordTxt.text?.count != 0 && usernameTxtCount != 0){
            loginBtn.isEnabled = true
        } else {
            loginBtn.isEnabled = false
        }
        passwordTxtCount = (passwordTxt.text?.count)!
    }
    
    @IBAction func login(_ sender: Any) {
                
        let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.labelText = "Loading"
        
        LoginViewModel.sharedInstance.loginUser(usernameStr: userNameTxt.text!, passwordStr: passwordTxt.text!) { (responseData) -> (Void) in
            print(responseData)
            let json = JSON(responseData)
            
            // hides acticity indicator
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
            
            let loginStatus = json.dictionary!["result"]?.bool as! Bool
            if (loginStatus) {

                let alert = UIAlertController(title: "Alert", message: "Authentication Success", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style:UIAlertActionStyle.default, handler: { action in
                    KeychainService.save(key:"AccessToken", token: json.dictionary!["auth_token"]!.string! as NSString)
                    
                    UserDefaults.standard.set(json.dictionary!["mqtt"]!["username"].string! as NSString, forKey: "username") //setObject
                    
                    // Perform Segue
                    self.performSegue(withIdentifier: "LogintoDashboardSegue", sender: self)
                    
                }))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                let alert = UIAlertController(title: "Alert", message: "Authentication Failed", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style:UIAlertActionStyle.default, handler: { action in
                }))
                self.present(alert, animated: true, completion: nil)
            }
            
            }
        
    }
    
    @IBAction func emailSupport(_ sender: Any) {
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
    }
    
// MARK: Touch ID methods - Starts
    func invokeTouchIDAuthentication() {
        var policy: LAPolicy?
        // Depending the iOS version we'll need to choose the policy we are able to use
        if #available(iOS 9.0, *) {
            // iOS 9+ users with Biometric and Passcode verification
            policy = .deviceOwnerAuthentication
        } else {
            // iOS 8+ users with Biometric and Custom (Fallback button) verification
            context.localizedFallbackTitle = "Fuu!"
            policy = .deviceOwnerAuthenticationWithBiometrics
        }
        
        var err: NSError?
        
        // Check if the user is able to use the policy we've selected previously
        guard context.canEvaluatePolicy(policy!, error: &err) else {
            // Print the localized message received by the system
            print(err?.localizedDescription)
            return
        }
        
        // Great! The user is able to use his/her Touch ID 👍
        print(kMsgShowFinger)
        
        touchIDAuthenticationProcess(policy: policy!)
    }

    
    private func touchIDAuthenticationProcess(policy: LAPolicy) {
        // Start evaluation process with a callback that is executed when the user ends the process successfully or not
        context.evaluatePolicy(policy, localizedReason: kMsgShowReason, reply: { (success, error) in
            DispatchQueue.main.async {
               
                
                guard success else {
                    guard let error = error else {
                        self.showUnexpectedErrorMessage()
                        return
                    }
                    switch(error) {
                    case LAError.authenticationFailed:
                        print("There was a problem verifying your identity.")
                    case LAError.userCancel:
                         print("Authentication was canceled by user.")
                        // Fallback button was pressed and an extra login step should be implemented for iOS 8 users.
                    // By the other hand, iOS 9+ users will use the pasccode verification implemented by the own system.
                    case LAError.userFallback:
                         print("The user tapped the fallback button (Fuu!)")
                    case LAError.systemCancel:
                         print("Authentication was canceled by system.")
                    case LAError.passcodeNotSet:
                         print("Passcode is not set on the device.")
                    case LAError.touchIDNotAvailable:
                         print("Touch ID is not available on the device.")
                    case LAError.touchIDNotEnrolled:
                         print("Touch ID has no enrolled fingers.")
                    // iOS 9+ functions
                    case LAError.touchIDLockout:
                         print("There were too many failed Touch ID attempts and Touch ID is now locked.")
                    case LAError.appCancel:
                         print("Authentication was canceled by application.")
                    case LAError.invalidContext:
                         print("LAContext passed to this call has been previously invalidated.")
                    default:
                         print("Touch ID may not be configured")
                        break
                    }
                    return
                }
                
                // Good news! Everything went fine 
                 print(self.kMsgFingerOK)
                
                // Perform Segue
                self.performSegue(withIdentifier: "LogintoDashboardSegue", sender: self)
            }
        })
    }
    
    private func showUnexpectedErrorMessage() {
         print("Unexpected error!")
    }
    
// MARK: Mail composer methods
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients(["someone@somewhere.com"])
        mailComposerVC.setSubject("Sending you an in-app e-mail...")
        mailComposerVC.setMessageBody("Sending e-mail in-app is not so bad!", isHTML: false)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: self, cancelButtonTitle: "OK")
        sendMailErrorAlert.show()
    }
    
// MFMailComposeViewControllerDelegate Method
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    

}

