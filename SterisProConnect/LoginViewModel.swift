//
//  LoginViewModel.swift
//  SterisProConnect
//
//  Created by Shri Harsha on 21/12/17.
//  Copyright © 2017 Shri Harsha. All rights reserved.
//

import UIKit

protocol LoginViewModelProtocol {
    
    associatedtype ResponseData
    
    func loginUser(username: String, password: String, completion: @escaping(Data)->(Void)) -> (Void)
}

class LoginViewModel: NSObject {
    
    var completeDataSet: [String:Any]?
    
    static let sharedInstance : LoginViewModel = LoginViewModel()
    private override init () {}
    typealias ResponseData = Data
    
    func loginUser (usernameStr: String, passwordStr: String, completion: @escaping (Data) -> (Void)){
        
        ApiService.sharedInstance.authenticateUser(username: usernameStr, password: passwordStr) { (responseData) -> (Void) in
            completion(responseData)
        }
        
    }
    
    func fetchDataDump (completion: ()->()){
        
//        ApiService.sharedInstance.fetchDataDump {_ in
//            
//            self.completeDataSet = Data
//            completion()
//        }
    }
    
    func numberItemsInSection(section: Int) -> Int{
        
        return 10 // completeDataSet?.count ?? 0
    }
    
    func titleForItemAtIndexPath (index: IndexPath) -> String {
        
        return "hi" //  completeDataSet?[index.row].valueForKeyPath("deviceName as per the response") as? String ?? ""
    }

}
