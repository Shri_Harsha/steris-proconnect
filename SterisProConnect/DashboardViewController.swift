//
//  DashboardViewController.swift
//  SterisProConnect
//
//  Created by Sreejith PA on 28/12/17.
//  Copyright © 2017 Altimetrik. All rights reserved.
//

import Foundation
import UIKit

class DashboardViewController: UIViewController {
    
@IBOutlet weak var titleLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        titleLbl.text =  UserDefaults.standard.string(forKey: "username")

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func logout(_ sender: Any) {
        KeychainService.delete(key: "AccessToken", token: "")
        
        UserDefaults.standard.set("", forKey: "username") //setObject
        
        // Perform Segue
        self.performSegue(withIdentifier: "DashboardtoLoginSegue", sender: self)
        
    }
    
    
}
